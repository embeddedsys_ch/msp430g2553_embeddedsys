/*1-Wire DS18B20 for MSP430
 * Author : Hai Dotcom
 * References : David Siroky's code
 * 				ytuongnhanh.vn
 * 				Datasheeet DS18B20
 * 	Created on: Mar 19,2017
 */
#include <msp430.h>
#include <stdint.h>
#include "MyDS18B20.h"
#include "UART_Config.h"

/*Using Clk =1MHz*/
/*The delay cycles are unstable, test and change until program completely work*/
/*________________________________________________________*/
/*
 * For 1wire sensors
 */
uint8_t onewire_reset()
{
	OW_LO
	_delay_cycles(550);	//480us minimum = 8 time slot
	OW_RLS				//Release line for resetting
	_delay_cycles(60); 	// Slave waits 15-60us
	if (OWIN & OWPIN)	// line should be pulled down by slave
		return 1;
	_delay_cycles(240); // Presence pulse signal from slave 60-240us
	if (!(OWIN & OWPIN))// line should be "released" by slave
		return 2;
	return 0;
	/*Summary
			 * 0 : Succeeded
			 * 1 : Failed - line wasn't pulled down
			 * 2 : Failed - Slave didn't send presence signal
	*/
}
/*******************************************************/
void onewire_port_setup()
{
	OWDIR |= OWPIN;
	OWOUT |= OWPIN;
	OWREN |= OWPIN;
}
/*******************************************************/
void onewire_writebit(uint8_t bit)
{
	_delay_cycles(2); // recovery
	OW_LO
	if(bit) //write 1
		_delay_cycles(6); //line was pulled down by master 1-15us
	else	//write 0
		_delay_cycles(64); //line was pulled up by master 60-120us
	/*Release line in rest of write slot*/
	OW_RLS
	if (bit) //1
		_delay_cycles(64);
	else 	//0
		_delay_cycles(6);
}

/*******************************************************/
uint8_t onewire_readbit()
{
	uint8_t bit =0;
	_delay_cycles(2); //recovery
	OW_LO
	_delay_cycles(5); //line was pulled down by master 0-15us
	OW_RLS
	_delay_cycles(6); //15us window - 15us sampling (according to the theory)
	if (OWIN & OWPIN)
		bit = 1; //read bit
	_delay_cycles(60);// rest of the read slot
	return bit;
}

/*******************************************************/
void onewire_wirte_byte(uint8_t byte)
{
	uint8_t i;
	for( i = 0; i < 8; i++)
	{
		onewire_writebit(byte & 1);
		byte >>=1;
	}
}

/******************************************************/
uint8_t onewire_read_byte()
{
	uint8_t i;
	uint8_t byte = 0;
	for(i = 0; i < 8; i++)
	{
		byte >>=1;
		if (onewire_readbit())
			byte |= 0x80;
	}
	return byte;
}

/******************************************************/
/*
 * For DS18B20 only
*/
float ReadDS18B20()
{
	uint8_t i;
	uint16_t byte = 0;
	for(i=0; i < 16 ; i++)
	{
		byte >>=1;
		if (onewire_readbit())
			byte |=0x8000;
	}
	return byte;
}

/***********************************************/
float Temp_GetData()
{
	uint16_t temp = 0;
	onewire_reset();
	onewire_wirte_byte(DS1820_SKIP_ROM);//write 0xcc-Skip ROM, address all devices
	onewire_wirte_byte(DS1820_CONVERT_T);//write 0x44-Start temperature conversion
	OW_HI
	_delay_cycles(750000); //delay 750ms for default 12-bit resolution
						   //according to datasheet DS18B20, page 3
	onewire_reset();
	onewire_wirte_byte(DS1820_SKIP_ROM);		//write 0xcc
	onewire_wirte_byte(DS1820_READ_SCRATCHPAD);	//write 0xbe-Read scratchpad(temp register)
	temp = ReadDS18B20();
	if(temp<0x8000){
	        return(temp*0.0625);
	    }
	    else
	    {
	        temp=(~temp)+1;
	        return(temp*0.0625);
	    }

	return temp;
}
/******************************************************/

