################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../lnk_msp430g2553.cmd 

C_SRCS += \
../ADC_config.c \
../MyDS18B20.c \
../UART_Config.c \
../main.c 

OBJS += \
./ADC_config.obj \
./MyDS18B20.obj \
./UART_Config.obj \
./main.obj 

C_DEPS += \
./ADC_config.d \
./MyDS18B20.d \
./UART_Config.d \
./main.d 

C_DEPS__QUOTED += \
"ADC_config.d" \
"MyDS18B20.d" \
"UART_Config.d" \
"main.d" 

OBJS__QUOTED += \
"ADC_config.obj" \
"MyDS18B20.obj" \
"UART_Config.obj" \
"main.obj" 

C_SRCS__QUOTED += \
"../ADC_config.c" \
"../MyDS18B20.c" \
"../UART_Config.c" \
"../main.c" 


