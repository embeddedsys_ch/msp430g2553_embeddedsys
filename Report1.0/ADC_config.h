#include "msp430.h"
#ifndef ADC_CONFIG_H_
#define ADC_CONFIG_H_

#define ADCEN_DIR	P2DIR
#define ADCEN_OUT	P2OUT
#define ADCEN_PIN	BIT1

#define ADC_EN	{ADCEN_OUT |= ADCEN_PIN;}
#define ADC_DIS	{ADCEN_OUT &= ~ADCEN_PIN;}

void ADC_Config();
unsigned int ADC_Getvalue(void);

#endif 
