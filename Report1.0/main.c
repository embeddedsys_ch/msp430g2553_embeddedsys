#include <msp430.h> 
#include "MyDS18B20.h"
#include "ADC_config.h"
#include <stdio.h>
/*
 * main.c
 */
int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
    int temp_data=0;
    int hum_data=0;
    char *temp_str,*hum_str;
    temp_str=(char*)calloc(10,sizeof(unsigned char));
    hum_str=(char*)calloc(10,sizeof(unsigned char));
    Uart_config();
    ADC_Config();
    //Enable PIN Configure
    ADCEN_DIR|=ADCEN_PIN;
    OWEN_DIR|=OWEN_PIN;
    OW_DIS
	ADC_DIS

    DCOCTL = 0;
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;
    onewire_port_setup();
        while (1)
        {
        	OW_EN
        	ADC_EN
        	temp_data = Temp_GetData();
        	hum_data=(100-(ADC_Getvalue()*100)/1023);
        	sprintf(temp_str,"%d Celsius",temp_data);
        	sprintf(hum_str,"%d",hum_data);
        	Uart_SendString("Temperature:");
        	Uart_SendString(temp_str);
        	Uart_SendString("\r\n");
        	Uart_SendString("Humidity:");
        	Uart_SendString(hum_str);
        	Uart_SendString("\r\n");
        	_delay_cycles(500000);
        }
	return 0;
}
