/*
 * UART_Config.c
 *
 *  Created on: Mar 9, 2017
 *      Author: Dell
 */

#include "UART_Config.h"


void Uart_config(void)
{
	P1SEL = BIT1 + BIT2;
	P1SEL2 = BIT1 + BIT2;
	UCA0CTL1 |= UCSWRST; /* USCI Software Reset */

	UCA0CTL0 = 0x00; //No parity bit, 8bit, 1 stop bit...

	UCA0CTL1 = UCSSEL_2 | UCSWRST; //* USCI 0 Clock Source: SMCLK */
	/*Configure Baud rate*/
	UCA0MCTL = UCBRF_8 | UCBRS_0 |UCOS16;
	UCA0BR0 = 6; // 1MHz . Baud rate = 9600. Following table 15-5
	UCA0BR1 = 00;


	UCA0CTL1 &= ~UCSWRST; // Reset module
	IE2 |= UCA0RXIE; // Enable USCI_A0 RX interrupt
	__bis_SR_register(GIE); // Interrupts enabled
}

unsigned char Uart_SendChar(unsigned char chr)
{
	while (!(IFG2 & UCA0TXIFG)); // Doi gui xong ky tu truoc
	UCA0TXBUF= chr; // Moi cho phep gui ky tu tiep theo
	return chr;
}
void Uart_SendString(char *string)
{
	while(*string) // Het chuoi ky tu thi thoat
			{
			while (!(IFG2&UCA0TXIFG)); // Doi gui xong ky tu truoc
			UCA0TXBUF= *string; // Moi cho phep gui ky tu tiep theo
			string ++; // Lay ky tu tiep theo
			}
		Uart_SendChar(0);
}
/*void Uart_SendInt(unsigned long n)
{

    unsigned char buffer[16];
    unsigned char i,j;

    if(n == 0) {
   	 Uart_SendChar('0');
         return;
    }

    for (i = 15; i > 0 && n > 0; i--) {
         buffer[i] = (n%10)+'0';
         n /= 10;
    }

    for(j = i+1; j <= 15; j++) {
   	 Uart_SendChar(buffer[j]);
    }
}

void Uart_SendFloat(float x, unsigned char sign)
{
	unsigned long temp;
		if(sign>4)sign=4;
		// de trong 1 ki tu o dau cho hien thi dau
		if(x<0)
		{
			Uart_SendChar('-');			//In so am
			x*=-1;
		}
		temp = (unsigned long)x;									// Chuyan thanh so nguyen.

		Uart_SendInt(temp);

		x=((float)x-temp);//*mysqr(10,sign);
		if(sign!=0)Uart_SendChar('.');	// In ra dau "."
		while(sign>0)
		{
			x*=10;
			sign--;
		}
		temp=(unsigned long)(x+0.5);	//Lam tron
		Uart_SendInt(temp);
}*/
char Uart_ReceiveChar()
{
	while (!(IFG2&UCA0RXIFG)); // Doi gui xong ky tu truoc
	return UCA0RXBUF; // Moi cho phep nhan ky tu tiep theo
}
void Uart_ReceiveString(char *s)
{
	*s = Uart_ReiceiveChar();
	while(*s!='\0')
	{
		s++;
		*s = Uart_ReceiveChar();
	}
}
char Uart_Ready()
{
	if(UCA0RXIFG) return 1;
		else return 0;
}

/*#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{
    while (!(IFG2 & UCA0TXIFG));                // USCI_A0 TX buffer ready?
    _delay_cycles(1000);
	UCA0TXBUF = UCA0RXBUF;                    // TX -> RXed character
	P1OUT|=BIT0;
    _delay_cycles(1000);
    P1OUT^=BIT0;
}*/
