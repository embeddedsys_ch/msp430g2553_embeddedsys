/*
 * UART_Config.h
 *
 *  Created on: Mar 9, 2017
 *      Author: Dell
 */
#include "msp430.h"
#include "stdio.h"
#ifndef UART_CONFIG_H_
#define UART_CONFIG_H_

#define BAUDRATE (9600)
#define SMCLK_F (1000000)

void Uart_config(void);
unsigned char Uart_SendChar(unsigned char chr);
void Uart_SendString(char *string);
void Uart_SendInt(unsigned long n);
void Uart_ReadFloat(float x, unsigned char sign);
char Uart_ReceiveChar();
void Uart_ReceiveString(char *s);
char Uart_Ready();



#endif /* UART_CONFIG_H_ */
