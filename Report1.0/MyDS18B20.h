/*
 * MyDS18B20.h
 *
 *  Created on: Mar 15, 2017
 *      Author: Hai Dotcom
 */

#ifndef MYDS18B20_H_
#define MYDS18B20_H_
#include <stdint.h>
#include <msp430g2553.h>

/*define onewire port*/

#define OWDIR 	P1DIR
#define OWOUT 	P1OUT
#define OWIN	P1IN
#define OWREN	P1REN
#define OWPIN	BIT3

#define OWEN_PIN	BIT0
#define OWEN_DIR	P2DIR
#define OWEN_OUT	P2OUT
/*define function------*/
#define OW_EN	{OWEN_OUT |= OWPIN;}
#define OW_DIS	{OWEN_OUT &= ~OWPIN;}
#define OW_LO	{OWDIR|=OWPIN; OWREN &= ~OWPIN; OWOUT &= ~OWPIN;}
#define OW_HI	{OWDIR|=OWPIN; OWREN &= ~OWPIN; OWOUT |= OWPIN;}
/*OW release: read slave stt*/
#define OW_RLS	{OWDIR &=~OWPIN; OWREN |=OWPIN; OWOUT |= OWPIN;/*Internal resistor pull up*/}

#define DS1820_SKIP_ROM             0xCC
#define DS1820_READ_SCRATCHPAD      0xBE
#define DS1820_CONVERT_T            0x44

/*Prototye------------------------------*/
uint8_t onewire_reset();
void onewire_port_setup();
void onewire_writebit(uint8_t bit);
uint8_t onewire_readbit();
void onewire_wirte_byte(uint8_t byte);
uint8_t onewire_read_byte();
/*for DS18B20*/
float Temp_GetData();
float ReadDS18B20();

#endif /* MYDS18B20_H_ */
