#include "main.h"


#include "ADC/ADC_config.h"
#include "DS18B20/MyDS18B20.h"
#include "UART/UART_Config.h"

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>


enum OPERATION_Mode MODE = IDLE;

void main(void)
{

    unsigned short sensorData[2];

    char *ADC_Value,*Temp_Value;
    ADC_Value = (char*)calloc(3,sizeof(unsigned char));
    Temp_Value = (char*)calloc(3,sizeof(unsigned char));

    SYSTEM_Init();

    while(1)
    {
        switch(MODE)
        {
        case IDLE:

        break;
        //ACLK ~ 1.5kHz
        case Sleep:
            TIMER0_Start
			OW_DIS
			ADC_DIS
			UART_DIS
            __bis_SR_register(LPM3_bits + GIE);
        break;
        //DCO ~ 1.1MHz
        case Wake:
        	OW_EN
			ADC_EN
			UART_EN
			_delay_cycles(300000);
            sensorData[0] = ADC_Getvalue();
        	sensorData[1] = (unsigned short)Temp_Data();
			sprintf(ADC_Value,"%d",sensorData[0]);
			sprintf(Temp_Value,"%d",sensorData[1]);
			Uart_SendString("\r\n");
			Uart_SendString("T:");
			Uart_SendString(Temp_Value);
			Uart_SendString("\r\n");
			Uart_SendString("H:");
			Uart_SendString(ADC_Value);
			Uart_SendString("\r\n");

			// ACLK/8 Very low power clk = 1.5kHz
			BCSCTL1 |= DIVA_3;
			// ACLK = VLO
			BCSCTL3 |= LFXT1S_2;

            MODE = Sleep;
        break;
        }
    }
}

//timer0 interrupt
#pragma vector = TIMER0_A0_VECTOR
__interrupt void myTimerISR(void)
{
    TIMER0_Stop                         // Stop Timer_A
    MODE = Wake;
    __bic_SR_register_on_exit(LPM3_bits);     // Clear LPM3 bits from 0(SR)
}

//init necessary things for system
void SYSTEM_Init(void)
{
    //stop watchdog
    WDTCTL = WDTPW + WDTHOLD;

    /* -------------------------------------------------------------- */
    //Timer
    // ACLK/8
    BCSCTL1 |= DIVA_3;
    // ACLK = VLO
    BCSCTL3 |= LFXT1S_2;

    /* -------------------------------------------------------------- */
    //UART
    Uart_config();
    UARTEN_DIR |= UARTEN_PIN;
    UART_DIS


    //ADC
    ADC_Config();
    ADCEN_DIR|=ADCEN_PIN;
    ADC_DIS

    /* -------------------------------------------------------------- */

    //temperature
    onewire_port_setup();
    OWEN_DIR|=OWEN_PIN;
    OW_DIS

    /* -------------------------------------------------------------- */

    // CCR0 interrupt enabled
    CCTL0 = CCIE;
    //delay time
    TACCR0 = TIMER0_DelayTime;
    // ACLK, upmode
    TACTL = TASSEL_1 + MC_1;

    /* -------------------------------------------------------------- */

    // Enter LPM3 + enable interrupt
    __bis_SR_register(LPM3_bits + GIE);
}
