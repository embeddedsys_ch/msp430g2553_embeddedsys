/*
 * ADC_config.c
 *
 *  Created on: Mar 7, 2017
 *      Author: Dell
 */
#include <msp430.h>
#include "ADC_config.h"

void ADC_Config(void)
{
	ADC10CTL1 = INCH_0 | ADC10SSEL_0;
	ADC10AE0 = BIT0;
	ADC10CTL0 = SREF_0;
	ADC10CTL0 |= ADC10ON + ENC;
}

unsigned int ADC_Getvalue(void)
{
	ADC10CTL0 |= ADC10SC;
	while (ADC10CTL1 & 1);
	return ADC10MEM;
}
