#include <msp430.h>

//operation mode
enum OPERATION_Mode
{
    IDLE,
    Sleep,
    Wake,
};

//define for timer
#define TIMER0_Start                    TACTL |= (MC_1);
#define TIMER0_Stop                     TACTL &=~(MC_1);
#define TIMER0_DelayTime                15000

__interrupt void myTimerISR(void);

void SYSTEM_Init(void);
